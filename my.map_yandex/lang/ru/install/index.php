<?
$MESS['MY_MODULE_INSTALL_NAME'] = 'Модуль-компонент Яндекс.Карт 1.0';
$MESS['MY_MODULE_INSTALL_TITLE'] = 'Установка модуля-компонента Яндекс.Карт 1.0';
$MESS['MY_MODULE_UNINSTALL_TITLE'] = 'Удаление модуля-компонента Яндекс.Карт 1.0';
$MESS['MY_MODULE_INSTALL_DESCRIPTION'] = 'Модуль позволяет установить компонент Яндекс.Карта (не типовой)';
$MESS['SPER_PARTNER'] = 'Роман Клёпов';
$MESS['PARTNER_URI'] = '';
$MESS['UF_MY_COMP_NAME_RU'] = 'Наименование';
$MESS['UF_MY_COMP_NAME_EN'] = 'Title';
$MESS['UF_MY_COMP_LAT_RU'] = 'Широта';
$MESS['UF_MY_COMP_LAT_EN'] = 'Latitude';
$MESS['UF_MY_COMP_LON_RU'] = 'Долгота';
$MESS['UF_MY_COMP_LON_EN'] = 'Longitude';
?>