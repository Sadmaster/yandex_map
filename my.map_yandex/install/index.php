<?
use \Bitrix\Main\Localization\Loc,
	\Bitrix\Main\ModuleManager,
	\Bitrix\Main\Application,
	\Bitrix\Main\IO\Directory as DirAction,
	\Bitrix\Highloadblock\HighloadBlockTable,
	\Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

if (class_exists('my_map_yandex'))
	return;

class my_map_yandex extends \CModule {
	
	// general vars
	public $MODULE_ID = 'my.map_yandex';
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	
	private $docRoot = '';
	private $componentName = 'my.map.yandex.view';
	
	
	// consructer
	public function __construct() {
		$arModuleVersion = array();
		
		$context = Application::getInstance()->getContext();
		$server = $context->getServer();
		$this->docRoot = $server->getDocumentRoot();
		
		if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion['VERSION'];
			$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		}

		$this->MODULE_NAME = Loc::getMessage('MY_MODULE_INSTALL_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('MY_MODULE_INSTALL_DESCRIPTION');
	}
	
	/**
	 * Call all install methods
	 * @returm void
	 */
	public function doInstall()
	{
		$this->installFiles();
		$this->installDB();
	}
	
	/**
	 * Install db
	 * @return boolean
	 */
	public function installDB()
	{
		if (Loader::includeModule('highloadblock')) {
			$resultAdd = HighloadBlockTable::add(array(
				'NAME' => 'MyCompData',
				'TABLE_NAME' => 'my_comp_data',
			));
			
			if (!$resultAdd->isSuccess())
				return false;
			else {
				$HLIblickId = $resultAdd->getId();

				// add user properties
				$oUserTypeEntity = new CUserTypeEntity();

				$arUFName = array(
					'ENTITY_ID' => 'HLBLOCK_' . $HLIblickId,
					'FIELD_NAME' => 'UF_MY_COMP_NAME',
					'USER_TYPE_ID' => 'string',
					'XML_ID' => 'XML_MY_COMP_NAME',
					'SORT' => 500,
					'MULTIPLE' => 'N',
					'MANDATORY' => 'N',
					'SHOW_FILTER' => 'N',
					'SHOW_IN_LIST' => '',
					'EDIT_IN_LIST' => '',
					'IS_SEARCHABLE' => 'N',
					'SETTINGS' => array(
						'DEFAULT_VALUE' => '',
						'SIZE' => '20',
						'ROWS' => '1',
						'MIN_LENGTH' => '0',
						'MAX_LENGTH' => '0',
						'REGEXP' => '',
					),
					'EDIT_FORM_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_NAME_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_NAME_EN'),
					),
					'LIST_COLUMN_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_NAME_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_NAME_EN'),
					),
					'LIST_FILTER_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_NAME_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_NAME_EN'),
					),
					'ERROR_MESSAGE' => array(
						'ru' => '',
						'en' => '',
					),
					'HELP_MESSAGE' => array(
						'ru' => '',
						'en' => '',
					),
				);
				 
				$arUFNameResId = $oUserTypeEntity->Add($arUFName);

				$arUFLat = array(
					'ENTITY_ID' => 'HLBLOCK_' . $HLIblickId,
					'FIELD_NAME' => 'UF_MY_COMP_LAT',
					'USER_TYPE_ID' => 'string',
					'XML_ID' => 'XML_MY_COMP_LAT',
					'SORT' => 500,
					'MULTIPLE' => 'N',
					'MANDATORY' => 'N',
					'SHOW_FILTER' => 'N',
					'SHOW_IN_LIST' => '',
					'EDIT_IN_LIST' => '',
					'IS_SEARCHABLE' => 'N',
					'SETTINGS' => array(
						'DEFAULT_VALUE' => '',
						'SIZE' => '20',
						'ROWS' => '1',
						'MIN_LENGTH' => '0',
						'MAX_LENGTH' => '0',
						'REGEXP' => '',
					),
					'EDIT_FORM_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_LAT_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_LAT_EN'),
					),
					'LIST_COLUMN_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_LAT_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_LAT_EN'),
					),
					'LIST_FILTER_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_LAT_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_LAT_EN'),
					),
					'ERROR_MESSAGE' => array(
						'ru' => '',
						'en' => '',
					),
					'HELP_MESSAGE' => array(
						'ru' => '',
						'en' => '',
					),
				);
				 
				$arUFLatResId = $oUserTypeEntity->Add($arUFLat);

				$arUFLon = array(
					'ENTITY_ID' => 'HLBLOCK_' . $HLIblickId,
					'FIELD_NAME' => 'UF_MY_COMP_LON',
					'USER_TYPE_ID' => 'string',
					'XML_ID' => 'XML_MY_COMP_LON',
					'SORT' => 500,
					'MULTIPLE' => 'N',
					'MANDATORY' => 'N',
					'SHOW_FILTER' => 'N',
					'SHOW_IN_LIST' => '',
					'EDIT_IN_LIST' => '',
					'IS_SEARCHABLE' => 'N',
					'SETTINGS' => array(
						'DEFAULT_VALUE' => '',
						'SIZE' => '20',
						'ROWS' => '1',
						'MIN_LENGTH' => '0',
						'MAX_LENGTH' => '0',
						'REGEXP' => '',
					),
					'EDIT_FORM_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_LON_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_LON_EN'),
					),
					'LIST_COLUMN_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_LON_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_LON_EN'),
					),
					'LIST_FILTER_LABEL' => array(
						'ru' => Loc::getMessage('UF_MY_COMP_LON_RU'),
						'en' => Loc::getMessage('UF_MY_COMP_LON_EN'),
					),
					'ERROR_MESSAGE' => array(
						'ru' => '',
						'en' => '',
					),
					'HELP_MESSAGE' => array(
						'ru' => '',
						'en' => '',
					),
				);
				 
				$arUFLonResId = $oUserTypeEntity->Add($arUFLon);

			}
		}
		
		ModuleManager::registerModule($this->MODULE_ID);
		return true;
	}
	
	/**
	 * Install files
	 * @return boolean
	 */
	public function installFiles()
	{
		try {
			copyDirFiles(
				$this->docRoot . $this->getPath() .'/modules/' . $this->MODULE_ID .'/install/components',
				$this->docRoot . $this->getPath() . '/components/',
				true, true
			);
		}
		catch(\Exception $e) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Call all uninstall methods
	 * @returm void
	 */
	public function doUninstall()
	{
		$this->uninstallFiles();
		$this->uninstallDB();
	}
	
	/**
	 * Uninstall db
	 * @return boolean
	 */
	public function uninstallDB()
	{
		ModuleManager::unregisterModule($this->MODULE_ID);
		
		if (Loader::includeModule('highloadblock')) {
			$dbHlIblocks = HighloadBlockTable::getList(
				array(
					'filter' => array(
						'TABLE_NAME' => 'my_comp_data'
					)
				)
			);
			
			if ($hldata = $dbHlIblocks->fetch())
				HighloadBlockTable::delete($hldata['ID']);
			else
				return false;			
		}
		
		return true;
	}
	
	/**
	 * Uninstall files
	 * @return boolean
	 */
	public function uninstallFiles()
	{
		try {
			DirAction::deleteDirectory($this->docRoot . $this->getPath() . '/components/my/' . $this->componentName);
		}
		catch (\Exception $e) {
			return false;
		}

		return true;
	}

	/**
	 * Auxiliary method
	 * @return string
	 */
	 private function getPath()
	 {
		 if (is_dir($this->docRoot . '/local/modules/' . $this->MODULE_ID . '/'))
			 return '/local/';
		 else
			 return '/bitrix/';
	 }
}
?>