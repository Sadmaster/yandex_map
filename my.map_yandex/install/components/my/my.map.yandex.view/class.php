<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class myYandexMap extends \CBitrixComponent
{
	/** @var preparing parameters */
	protected function prepareParams()
	{
		$this->arParams['MAP_ID'] = (strlen($this->arParams["MAP_ID"]) <= 0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $this->arParams["MAP_ID"])) ? 
		'MAP_'.$this->randString() : $this->arParams['MAP_ID'];
	}
	
	/** @array preparing result */
	protected function prepareResult()
	{
		if (($strPositionInfo = $this->arParams['~MAP_DATA']) && CheckSerializedData($strPositionInfo) && ($this->arResult['POSITION'] = unserialize($strPositionInfo)))
		{
			if (is_array($this->arResult['POSITION']) && is_array($this->arResult['POSITION']['PLACEMARKS']) && ($cnt = count($this->arResult['POSITION']['PLACEMARKS'])))
			{
				for ($i = 0; $i < $cnt; $i++)
				{
					$this->arResult['POSITION']['PLACEMARKS'][$i]['TEXT'] = str_replace('###RN###', "\r\n", $this->arResult['POSITION']['PLACEMARKS'][$i]['TEXT']);
				}
			}

			if (is_array($this->arResult['POSITION']) && is_array($this->arResult['POSITION']['POLYLINES']) && ($cnt = count($this->arResult['POSITION']['POLYLINES'])))
			{
				for ($i = 0; $i < $cnt; $i++)
				{
					$this->arResult['POSITION']['POLYLINES'][$i]['TITLE'] = str_replace('###RN###', "\r\n", $this->arResult['POSITION']['POLYLINES'][$i]['TITLE']);
				}
			}
		}
	}
	
	// execute component
	public function executeComponent()
	{
		$this->prepareParams();
		$this->prepareResult();
		$this->includeComponentTemplate();
	}
}
?>