<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	'NAME' => Loc::getMessage('MY_COMP_NAME'),
	'DESCRIPTION' => Loc::getMessage('MY_COMP_DESCRIPTION'),
	'ICON' => '/images/map_view.gif',
	'CACHE_PATH' => 'Y',
	'PATH' => array(
		'ID' => 'content',
		'NAME' => Loc::getMessage('MY_Y_CONTENT'),
		'CHILD' => array(
			'ID' => 'yandex_map',
			'NAME' => Loc::getMessage('MY_YANDEX_MAP_SERVICE'),
		)
	),
);

?>