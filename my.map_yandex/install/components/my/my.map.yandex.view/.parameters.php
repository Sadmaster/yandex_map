<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
	\Bitrix\Main\Application,
	\Bitrix\Main\Loader,
	\Bitrix\Highloadblock\HighloadBlockTable;

$context = Application::getInstance()->getContext();
$server = $context->getServer();
$docRoot = $server->getDocumentRoot();

$compPath = (is_dir($docRoot . '/local/components/my/my.map.yandex.view/')) ? '/local/components/my/my.map.yandex.view/' : '/bitrix/components/my/my.map.yandex.view/';

if (Loader::includeModule('highloadblock')) {
	$HLId = 0;
	$dbHlIblocks = HighloadBlockTable::getList(
		array(
			'filter' => array(
				'TABLE_NAME' => 'my_comp_data'
			)
		)
	);

	if ($hldata = $dbHlIblocks->fetch())
		$HLId = $hldata['ID'];
}

$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(
		'HLIBLOCK_ID' => array(
			'NAME' => Loc::getMessage('MY_COMP_PARAM_MAP_HLIBLOCK_ID'),
			'TYPE' => 'STRING',
			'DEFAULT' => '500',
			'PARENT' => 'BASE',
			'DEFAULT' => ($HLId) ? $HLId : 0
		),
		'INIT_MAP_TYPE' => array(
			'NAME' => Loc::getMessage('MY_COMP_PARAM_INIT_MAP_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => array(
				'MAP' => Loc::getMessage('MY_COMP_PARAM_INIT_MAP_TYPE_MAP'),
				'SATELLITE' => Loc::getMessage('MY_COMP_PARAM_INIT_MAP_TYPE_SATELLITE'),
				'HYBRID' => Loc::getMessage('MY_COMP_PARAM_INIT_MAP_TYPE_HYBRID'),
				'PUBLIC' => Loc::getMessage('MY_COMP_PARAM_INIT_MAP_TYPE_PUBLIC'),
				'PUBLIC_HYBRID' => Loc::getMessage('MY_COMP_PARAM_INIT_MAP_TYPE_PUBLIC_HYBRID'),
			),
			'DEFAULT' => 'MAP',
			'ADDITIONAL_VALUES' => 'N',
			'PARENT' => 'BASE',
		),

		'MAP_DATA' => array(
			'NAME' => Loc::getMessage('MY_COMP_PARAM_DATA'),
			'TYPE' => 'CUSTOM',
			'JS_FILE' => $compPath . 'settings/settings.js',
			'JS_EVENT' => 'OnYandexMapSettingsEdit',
			'JS_DATA' => LANGUAGE_ID.'||'.Loc::getMessage('MY_COMP_PARAM_DATA_SET').'||'.Loc::getMessage('MY_COMP_PARAM_DATA_NO_KEY').'||'.Loc::getMessage('MY_COMP_PARAM_DATA_GET_KEY').'||'.Loc::getMessage('MY_COMP_PARAM_DATA_GET_KEY_URL'),
			'DEFAULT' => serialize(array(
				'yandex_lat' => Loc::getMessage('MY_COMP_PARAM_DATA_DEFAULT_LAT'),
				'yandex_lon' => Loc::getMessage('MY_COMP_PARAM_DATA_DEFAULT_LON'),
				'yandex_scale' => 10
			)),
			'PARENT' => 'BASE',
		),

		'MAP_WIDTH' => array(
			'NAME' => Loc::getMessage('MY_COMP_PARAM_MAP_WIDTH'),
			'TYPE' => 'STRING',
			'DEFAULT' => '600',
			'PARENT' => 'BASE',
		),

		'MAP_HEIGHT' => array(
			'NAME' => Loc::getMessage('MY_COMP_PARAM_MAP_HEIGHT'),
			'TYPE' => 'STRING',
			'DEFAULT' => '500',
			'PARENT' => 'BASE',
		),

		'CONTROLS' => array(
			'NAME' => Loc::getMessage('MY_COMP_PARAM_CONTROLS'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'VALUES' => array(
				/*'TOOLBAR' => Loc::getMessage('MY_COMP_PARAM_CONTROLS_TOOLBAR'),*/
				'ZOOM' => Loc::getMessage('MY_COMP_PARAM_CONTROLS_ZOOM'),
				'SMALLZOOM' => Loc::getMessage('MY_COMP_PARAM_CONTROLS_SMALLZOOM'),
				'MINIMAP' => Loc::getMessage('MY_COMP_PARAM_CONTROLS_MINIMAP'),
				'TYPECONTROL' => Loc::getMessage('MY_COMP_PARAM_CONTROLS_TYPECONTROL'),
				'SCALELINE' => Loc::getMessage('MY_COMP_PARAM_CONTROLS_SCALELINE'),
				'SEARCH' => Loc::getMessage('MY_COMP_PARAM_CONTROLS_SEARCH'),
			),

			'DEFAULT' => array(/*'TOOLBAR', */'ZOOM', 'MINIMAP', 'TYPECONTROL', 'SCALELINE'),
			'PARENT' => 'ADDITIONAL_SETTINGS',
		),

		'OPTIONS' => array(
			'NAME' => Loc::getMessage('MY_COMP_PARAM_OPTIONS'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'VALUES' => array(
				'ENABLE_SCROLL_ZOOM' => Loc::getMessage('MY_COMP_PARAM_OPTIONS_ENABLE_SCROLL_ZOOM'),
				'ENABLE_DBLCLICK_ZOOM' => Loc::getMessage('MY_COMP_PARAM_OPTIONS_ENABLE_DBLCLICK_ZOOM'),
				'ENABLE_RIGHT_MAGNIFIER' => Loc::getMessage('MY_COMP_PARAM_OPTIONS_ENABLE_RIGHT_MAGNIFIER'),
				'ENABLE_DRAGGING' => Loc::getMessage('MY_COMP_PARAM_OPTIONS_ENABLE_DRAGGING'),
				/*'ENABLE_HOTKEYS' => Loc::getMessage('MY_COMP_PARAM_OPTIONS_ENABLE_HOTKEYS'),*/
				/*'ENABLE_RULER' => Loc::getMessage('MY_COMP_PARAM_OPTIONS_ENABLE_RULER'),*/
			),


			'DEFAULT' => array('ENABLE_SCROLL_ZOOM', 'ENABLE_DBLCLICK_ZOOM', 'ENABLE_DRAGGING'),
			'PARENT' => 'ADDITIONAL_SETTINGS',
		),

		'MAP_ID' => array(
			'NAME' => Loc::getMessage('MY_COMP_PARAM_MAP_ID'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
			'PARENT' => 'ADDITIONAL_SETTINGS',
		),
	),
);
?>